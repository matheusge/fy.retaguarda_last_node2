/*
    repository/lib/clienteModel.js
*/

'use strict'
var config = require('../../infra');

var m = config.getConnection().model('Cliente', {
    nome: String,
    cpf: String ,
    celular: String,
    email: String,
    totalpontos: Number,
    dtnascimento: Date,
    genero: String
});

exports = m; 
