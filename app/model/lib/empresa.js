/*
    ./app/model/lib/empresa.js
*/

'use strict'
var config = require('../../infra');

var m = config.getConnection().model('Empresa', {
    nome: String,
    cnpj: String , //CPJ terá de ser único
    
    msgblock: String, //Mensagem que irá aparecer na tela de bloqueio do aplicativo

    pontoscompra: Number, //Configuração de pontos para cada compra concluída
    
    pontoscartela: Number, //Total de pontos para cartela digital. Quando cartela ativa, desativar configpontos
                          // Será 1 ponto por compra

    cartelaativa: Boolean //Define se o modo de pontuação "Cartela Digital" está ativo

    //imagem
});

// m.prototype.Compare = function(e){
//     if(this.nome != e.nome){ this.nome = e.nome; };
//     if(this.cnpj != e.cnpj){ this.cnpj = e.cnpj; };
//     if(this.config_pontos != e.config_pontos){ this.config_pontos = e.config_pontos; };
// };

exports = m;