/*
    ./app/service/lib/empresa.js
*/

'use strict'
var model = require('../../model');
var generic = require('./generic');

exports.Create = function(){
    var _generic = new generic.Create('Empresa', model.empresa.models);
    for(var n in _generic) { this[n] = _generic[n] };
};