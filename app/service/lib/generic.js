/*
    ./app/service/lib/generic.js
*/

var infra = require('../../infra');
// var _ = require('underscore');

exports.Create = function(model_name, model_object){

    var _modelName = model_name;
    var _modelObject = model_object;

    this.getAll = function(callback){
        var m = infra.getConnection().model(_modelName, _modelObject);
        m.find(function(err, r){
             if(err){
                callback(err);
            }
            callback(r);
            return;
         });
    };
};

