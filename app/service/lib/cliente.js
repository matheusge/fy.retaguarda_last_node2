/*
    ./app/service/lib/cliente.js
*/

'use strict'
var model = require('../../model');
var generic = require('./generic');

exports.Create = function(){
    var _generic = new generic.Create('Cliente', model.cliente.models);
    for(var n in _generic) { this[n] = _generic[n] };
};