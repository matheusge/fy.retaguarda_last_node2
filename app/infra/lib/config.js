/*
    ./app/config/lib/config.js
*/

'use strict'
var mongoose = require('mongoose');
var _config = require('../config.json');

var Configuracao = function(){
    this.mongoConnection = _config.connectionMDB;
    this._conexao = null;

    this.createConnection = function(){
        var self = this;
        mongoose.connect(_config.connectionMDB, function(error){
            if(error) console.log(error);
            console.log("Conexão feita com sucesso!");
        });
        self._conexao = mongoose;
    };

    this.getConnection = function(){
        var self = this;
        if(self._conexao == null){
            self.createConnection();
        }
        return self._conexao;
    };
};

module.exports = new Configuracao();