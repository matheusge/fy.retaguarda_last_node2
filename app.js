/*
    app.js
*/

var app = require('./config/express')();
var porta = 8080;

app.listen(porta, function(){
    console.log('Servidor rodando: ' + porta);
});