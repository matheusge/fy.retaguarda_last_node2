/*
	./config/express.js
*/

var express = require('express');
var load = require('express-load');
var bodyParser = require('body-parser');
// var _ = require('underscore');

var app = express();

app.set('view engine', 'ejs');
app.set('views', './app/views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

module.exports = function(){
    load('route', { cwd: 'app' })
        // .then('dto')
        //.then('controller')
        .into(app);

	return app;
};